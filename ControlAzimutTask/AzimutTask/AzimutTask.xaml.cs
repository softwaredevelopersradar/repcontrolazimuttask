﻿using System.Windows;
using System.Windows.Controls;
using System;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Windows.Data;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using AzimutTask.Models;
using AzimutTask.Editors;
using AzimutTask.Enums;

namespace AzimutTask
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class AzimutTask : UserControl, INotifyPropertyChanged
    {
        public AzimutTask()
        {
            InitializeComponent();
            SetStartValue();
            ChangeLanguge(language);
            SetBindingIsShow();
            InitEditors();
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private string pathTranslation = System.IO.Directory.GetCurrentDirectory() + "\\Languages\\TranslationAzimutTask.xml";

        private string pathResourceEng = "/AzimutTask;component/Languages/StringResource.EN.xaml";

        private string pathResourceRus = "/AzimutTask;component/Languages/StringResource.xaml";

        private string pathResourceAz = "/AzimutTask;component/Languages/StringResource.AZ.xaml";

        EnumSysCoord currentSysCoord;

        CoordDegree modelDegree = new CoordDegree();
        CoordDdMmSs modelDdMmSs = new CoordDdMmSs();
        CoordXY modelXY = new CoordXY();
        Languages language = Languages.Eng;

        #endregion

        #region Events

        public event EventHandler<ICoord> OnExecute = (sender, obj) => { };

        public event EventHandler OnClear = (sender, obj) => { };

        #endregion

        #region Properties

        public CoordDegree ModelDegree
        {
            get => modelDegree;
            private set
            {
                if (modelDegree == value) return;
                modelDegree = value;
                OnPropertyChanged();
            }
        }

        public CoordDdMmSs ModelDdMmSs
        {
            get => modelDdMmSs;
            private set
            {
                if (modelDdMmSs == value) return;
                modelDdMmSs = value;
                OnPropertyChanged();
            }
        }

        public CoordXY ModelXY
        {
            get => modelXY;
            private set
            {
                if (modelXY == value) return;
                modelXY = value;
                OnPropertyChanged();
            }
        }
        
        public bool IsShowValue
        {
            get => (bool)GetValue(IsShowValueProperty);
            set { SetValue(IsShowValueProperty, value); }
        }
        
        #endregion

        #region DependencyProperty
        
        public static readonly DependencyProperty IsShowValueProperty = DependencyProperty.Register(nameof(IsShowValue), typeof(bool), typeof(AzimutTask), new FrameworkPropertyMetadata(false, IsShowValuePropertyChanged));
        
        private static void IsShowValuePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            AzimutTask model = (AzimutTask)sender;
            if (e.OldValue != e.NewValue)
            {
                model.IsShowValue = (bool)e.NewValue;
                model.OnPropertyChanged(nameof(model.IsShowValue));
            }
        }

        #endregion

        #region Language

        public void ChangeLanguge(Languages language)
        {
            this.language = language;
            var translation = LoadDictionary(language);
            if (translation != null)
                UpdateNameProperty(translation);
            SetResourcelanguage(language);
        }

        private void UpdateNameCategory(Dictionary<string, string> TranslateDic)
        {
            try
            {
                foreach (var category in PropertyTaskAzimut.Categories)
                {
                    if (TranslateDic.ContainsKey(category.Name))
                        PropertyTaskAzimut.Categories.First(t => t.Name == category.Name).HeaderCategoryName = TranslateDic[category.Name];
                }
            }
            catch (Exception)
            {

            }
        }

        private void UpdateNameProperty(Dictionary<string,string> TranslateDic)
        {
            try
            {
                foreach (var property in PropertyTaskAzimut.Properties)
                {
                    if (TranslateDic.ContainsKey(property.Name))
                        PropertyTaskAzimut.Properties.First(t => t.Name == property.Name).DisplayName = TranslateDic[property.Name];
                }
            }
            catch (Exception)
            {

            }
        }

        private void SetResourcelanguage(Languages lang)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (lang)
                {
                    case Languages.Eng:
                        dict.Source = new Uri(pathResourceEng, UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri(pathResourceRus, UriKind.Relative);
                        break;
                    case Languages.Az:
                        dict.Source = new Uri(pathResourceAz, UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri(pathResourceRus, UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }

        private Dictionary<string, string> LoadDictionary(Languages lang)
        {
            XmlDocument xDoc = new XmlDocument();
            if (System.IO.File.Exists(pathTranslation))
                xDoc.Load(pathTranslation);
            else
            {
                switch (lang)
                {

                    case Languages.Rus:
                        // MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!");
                        break;
                    case Languages.Eng:
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!");
                        break;
                    case Languages.Az:
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!");
                        break;
                    default:
                        // MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!");
                        break;
                }

                return null;
            }
            Dictionary<string, string> TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == lang.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }

                        }
                    }
                }
            }
            return TranslateDic;
        }
        #endregion

        #region Private Function

        private void SetStartValue()
        {
            currentSysCoord = EnumSysCoord.Degree;
            PropertyTaskAzimut.SelectedObject = ModelDegree;
        }

        private void SetBindingIsShow()
        {
            Binding binding = new Binding();
            binding.Source = this;
            binding.Path = new PropertyPath("IsShowValue");
            binding.Mode = BindingMode.TwoWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            ChekIsShowValue.SetBinding(CheckBox.IsCheckedProperty, binding);
        }

        private void ButClear_Click(object sender, RoutedEventArgs e)
        {
            OnClear(sender, e);
        }

        private void ButExecute_Click(object sender, RoutedEventArgs e)
        {
            ICoord coord;
            switch(currentSysCoord)
            {
                case EnumSysCoord.DdMmSs:
                    coord = ModelDdMmSs;
                    break;
                case EnumSysCoord.Degree:
                    coord = ModelDegree;
                    break;
                case EnumSysCoord.XY:
                    coord = ModelXY;
                    break;
                default:
                    coord = null;
                    break;
            }
            OnExecute(sender, coord);
        }

        private void InitEditors()
        {
            PropertyTaskAzimut.Editors.Add(new EditorCoord(nameof(CoordDdMmSs.Latitude), typeof(CoordDdMmSs), ModelDdMmSs.Latitude.GetType()));
            PropertyTaskAzimut.Editors.Add(new EditorCoord(nameof(CoordDdMmSs.Longitude), typeof(CoordDdMmSs), ModelDdMmSs.Longitude.GetType()));
            PropertyTaskAzimut.Editors.Add(new EditorCoord(nameof(CoordDdMmSs.Altitude), typeof(CoordDdMmSs), ModelDdMmSs.Altitude.GetType()));

            PropertyTaskAzimut.Editors.Add(new EditorCoord(nameof(CoordDegree.Altitude), typeof(CoordDegree), ModelDegree.Altitude.GetType()));
            PropertyTaskAzimut.Editors.Add(new EditorCoord(nameof(CoordDegree.Latitude), typeof(CoordDegree), ModelDegree.Latitude.GetType()));
            PropertyTaskAzimut.Editors.Add(new EditorCoord(nameof(CoordDegree.Longitude), typeof(CoordDegree), ModelDegree.Longitude.GetType()));

            PropertyTaskAzimut.Editors.Add(new EditorCoord(nameof(CoordXY.Altitude), typeof(CoordXY), ModelXY.Altitude.GetType()));
            PropertyTaskAzimut.Editors.Add(new EditorCoord(nameof(CoordXY.X), typeof(CoordXY), ModelXY.X.GetType()));
            PropertyTaskAzimut.Editors.Add(new EditorCoord(nameof(CoordXY.Y), typeof(CoordXY), ModelXY.Y.GetType()));

        }

        #endregion

        #region Public Function

        public void ChangeSystemCoord(EnumSysCoord newSysCoord)
        {
            if (currentSysCoord == newSysCoord) return;
            currentSysCoord = newSysCoord;
            try
            {
                switch (currentSysCoord)
                {
                    case EnumSysCoord.Degree:
                        ModelDegree = new CoordDegree();
                        PropertyTaskAzimut.SelectedObject = ModelDegree;
                        break;

                    case EnumSysCoord.DdMmSs:
                        ModelDdMmSs = new CoordDdMmSs();
                        PropertyTaskAzimut.SelectedObject = ModelDdMmSs;
                        break;
                    case EnumSysCoord.XY:
                        ModelXY = new CoordXY();
                        PropertyTaskAzimut.SelectedObject = ModelXY;
                        break;
                }

                ChangeLanguge(language);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetModel(ICoord model)
        {
            try
            {
                switch (currentSysCoord)
                {
                    case EnumSysCoord.Degree:
                        ModelDegree = (model as CoordDegree) ?? ModelDegree;
                        PropertyTaskAzimut.SelectedObject = ModelDegree;
                        break;

                    case EnumSysCoord.DdMmSs:
                        ModelDdMmSs = (model as CoordDdMmSs) ?? ModelDdMmSs;
                        PropertyTaskAzimut.SelectedObject = ModelDdMmSs;
                        break;
                    case EnumSysCoord.XY:
                        ModelXY = (model as CoordXY)?? ModelXY;
                        PropertyTaskAzimut.SelectedObject = ModelXY;
                        break;
                }

            }
            catch(Exception ex)
            {

            }
        }

        #endregion
    }
}
