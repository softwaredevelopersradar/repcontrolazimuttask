﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AzimutTask
{
    public class ConvertDouble : IValueConverter
    {
        private readonly char separator = '.';

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string source = value.ToString().Replace(',', separator);
            return source;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string source = value.ToString().Replace(',', separator);
            NumberStyles style = NumberStyles.Number | NumberStyles.AllowCurrencySymbol;
            culture = CultureInfo.CreateSpecificCulture("en-GB");
            double result;
            if (Double.TryParse(source,  style, culture, out result))
            {
                return result;
            }
            return null;
        }
    }
}
