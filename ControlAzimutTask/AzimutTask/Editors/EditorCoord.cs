﻿using AzimutTask.Models;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace AzimutTask.Editors
{
    class EditorCoord : PropertyEditor
    {
        Dictionary<string, string> KeyDataTempCoordDdMmSs = new Dictionary<string, string>
        {
            {nameof(CoordDdMmSs.Latitude), "ItemCoordDdMmSsEditorKey" },
            {nameof(CoordDdMmSs.Longitude), "ItemCoordDdMmSsEditorKey" },
            {nameof(ICoord.Altitude), "CoordMetersEditorKey" }
        };

        Dictionary<string, string> KeyDataTempCoordDegree = new Dictionary<string, string>
        {
            {nameof(ICoord.Altitude), "CoordMetersEditorKey" },
            {nameof(CoordDegree.Latitude), "CoordDegreeEditorKey" },
            {nameof(CoordDegree.Longitude), "CoordDegreeEditorKey" }
        };

        Dictionary<string, string> KeyDataTempCoordXY = new Dictionary<string, string>
        {
            {nameof(ICoord.Altitude), "CoordMetersEditorKey" },
            {nameof(CoordXY.X), "CoordMetersEditorKey" },
            {nameof(CoordXY.Y), "CoordMetersEditorKey" }
        };

        string pathUri = "/AzimutTask;component/View/Dictionary.xaml";

        public EditorCoord(string PropertyName, Type DeclaringType, Type typeProperty)
        {
            Dictionary<string, string> dictKeyTemp = GetDictionaryTemp(DeclaringType);
            if (dictKeyTemp == null) return;
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri(pathUri, UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyTemp[PropertyName]];
        }

        private Dictionary<string, string> GetDictionaryTemp(Type DeclaringType)
        {
            if (DeclaringType == typeof(CoordDegree))
                return KeyDataTempCoordDegree;
            else if (DeclaringType == typeof(CoordDdMmSs))
                return KeyDataTempCoordDdMmSs;
            else if (DeclaringType == typeof(CoordXY))
                return KeyDataTempCoordXY;
            return null;
        }
    }
}
