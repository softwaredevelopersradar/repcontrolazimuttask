﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Linq;

namespace AzimutTask
{
    public static class TextBoxExtend
    {
        #region OnTyping

        public static readonly DependencyProperty CommitOnDoubleTypingProperty = DependencyProperty.RegisterAttached("CommitOnDoubleTyping", typeof(bool), typeof(TextBoxExtend), new FrameworkPropertyMetadata(false, OnCommitOnDoubleTypingChanged));

        private static void OnCommitOnDoubleTypingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
            {
                textbox.KeyUp -= TextBoxCommitValueWhileTyping;
                textbox.PreviewTextInput -= TextboxCommitPreviewTextInput;
                
            }

            if (needToBind)
            {
                textbox.PreviewTextInput += TextboxCommitPreviewTextInput;
                textbox.KeyUp += TextBoxCommitValueWhileTyping;
            }
        }

        private static void TextboxCommitPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789,.".IndexOf(e.Text) < 0;
        }

        static void TextBoxCommitValueWhileTyping(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemMinus || e.Key == Key.Subtract || e.Key == Key.Escape) //e.Key==Key.OemComma||
                return;

            var textbox = sender as TextBox;

            if (textbox == null) return;

            if (textbox.Text == "") return;

            var eS = textbox.Text[textbox.Text.Length - 1];
            if (textbox.Text.Contains(",") || textbox.Text.Contains("."))
            {
                string text = textbox.Text.Remove(textbox.Text.Length - 1, 1);

                if (!(text.Contains(",") || text.Contains(".")))
                    return;

                textbox.Text = SubstituteSeparator(textbox.Text);

                textbox.SelectionStart = textbox.Text.Length;
                textbox.SelectionLength = 0;


                if (e.Key == Key.D0 || e.Key == Key.NumPad0)
                    return;
            }

            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null) expression.UpdateSource();
            e.Handled = true;
        }
       
        private static string SubstituteSeparator(string text)
        {
            char separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
            string Source = text.Replace(',', separator);
            Source = Source.Replace('.', separator);
            Source = RemoveAdditionalSepparators(Source, separator);
            return Source;
        }

        private static string RemoveAdditionalSepparators(string text, char separator)
        {
            int index = text.IndexOf(separator);
            text = text.Trim(separator);
            if (!text.Contains(separator))
                text = text.Insert(index, separator.ToString());
            return text;
        }

        public static void SetCommitOnDoubleTyping(TextBox target, bool value)
        {
            target.SetValue(CommitOnDoubleTypingProperty, value);
        }

        public static bool GetCommitOnDoubleTyping(TextBox target)
        {
            return (bool)target.GetValue(CommitOnDoubleTypingProperty);
        }
        #endregion

    }
}
