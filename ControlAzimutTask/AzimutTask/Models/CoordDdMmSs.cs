﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AzimutTask.Models
{
    public class CoordDdMmSs : INotifyPropertyChanged, ICoord
    {
        public CoordDdMmSs()
        {
            Longitude = new ItemCoordDdMmSs();

            Latitude = new ItemCoordDdMmSs();
        }
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        
        #endregion

        #region Catefories

        public const string categoryLongitude = "Longitude";

        public const string categoryLatitude = "Latitude";
        
        #endregion

        #region Private

        private ItemCoordDdMmSs latitude;

        private ItemCoordDdMmSs longitude;

        private double altitude;

        #endregion

        #region Property

        public double Altitude
        {
            get => altitude;
            set
            {
                if (altitude == value) return;
                altitude = value;
                OnPropertyChanged();
            }
        }

        [Category(categoryLatitude)]
        public ItemCoordDdMmSs Latitude
        {
            get => latitude;
            set
            {
                if (latitude == value) return;
                latitude = value;
                OnPropertyChanged();
            }
        }

        [Category(categoryLongitude)]
        public ItemCoordDdMmSs Longitude
        {
            get => longitude;
            set
            {
                if (longitude == value) return;
                longitude = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}
