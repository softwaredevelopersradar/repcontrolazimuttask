﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AzimutTask.Models
{
    public class CoordDegree: INotifyPropertyChanged, ICoord
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region private

        private double latitude;

        private double longitude;

        private double altitude;

        #endregion

        public double Latitude
        {
            get => latitude;
            set
            {
                if (latitude == value) return;
                latitude = value;
                OnPropertyChanged();
            }
        }

        public double Longitude
        {
            get => longitude;
            set
            {
                if (longitude == value) return;
                longitude = value;
                OnPropertyChanged();
            }
        }

        public double Altitude
        {
            get => altitude;
            set
            {
                if (altitude == value) return;
                altitude = value;
                OnPropertyChanged();
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
