﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AzimutTask.Models
{
    public class CoordXY : INotifyPropertyChanged, ICoord
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region private

        private double xValue;

        private double yValue;

        private double altitude;

        #endregion

        public double X
        {
            get => xValue;
            set
            {
                if (xValue == value) return;
                xValue = value;
                OnPropertyChanged();
            }
        }

        public double Y
        {
            get => yValue;
            set
            {
                if (yValue == value) return;
                yValue = value;
                OnPropertyChanged();
            }
        }

        public double Altitude
        {
            get => altitude;
            set
            {
                if (altitude == value) return;
                altitude = value;
                OnPropertyChanged();
            }
        }

    }
}
