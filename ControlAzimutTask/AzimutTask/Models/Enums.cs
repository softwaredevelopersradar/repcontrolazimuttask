﻿namespace AzimutTask.Enums
{
    public enum EnumSysCoord
    {
        XY,
        DdMmSs,
        Degree
    }

    public enum Languages
    {
        Rus,
        Eng,
        Az
    }
}
