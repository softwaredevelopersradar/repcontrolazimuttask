﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzimutTask.Models
{
    public interface ICoord
    {
        double Altitude { get; set; }
    }
}
