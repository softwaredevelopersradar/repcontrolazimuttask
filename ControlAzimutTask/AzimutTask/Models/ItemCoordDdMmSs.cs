﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AzimutTask.Models
{

    public class ItemCoordDdMmSs : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region private 

        private double degree;

        private double minutes;

        private double seconds;

        #endregion

        [NotifyParentProperty(true)]
        public double Degree
        {
            get => degree;
            set
            {
                if (degree == value) return;
                degree = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public double Minutes
        {
            get => minutes;
            set
            {
                if (minutes == value) return;
                minutes = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public double Seconds
        {
            get => seconds;
            set
            {
                if (seconds == value) return;
                seconds = value;
                OnPropertyChanged();
            }
        }


    }
}
