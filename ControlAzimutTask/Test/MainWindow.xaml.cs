﻿using AzimutTask;
using AzimutTask.Enums;
using AzimutTask.Models;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Test
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = null)
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        private bool isShow;
        public bool IsShowTest
        {
            get => isShow;
            set
            {
                if (value == isShow) return;
                isShow = value;
                OnPropertyChanged();
            }
        }


        private RelayCommand commandExecute = new RelayCommand(obj => { });
        public RelayCommand CommandExecute
        {
            get => commandExecute;
            set
            {
                commandExecute = value;
                OnPropertyChanged();
            }
        }

        public MainWindow()
        {
            DataContext = this;
            
            InitializeComponent();
            testAzimutTask.OnClear += TestAzimutTask_OnClear;
            testAzimutTask.OnExecute += TestAzimutTask_OnExecute;
            SetResourcelanguage(Languages.Rus);
        }

        private void TestAzimutTask_OnExecute(object sender, ICoord e)
        {
            if (e.GetType() == typeof(CoordXY))
            {
                CoordXY modelXY = e as CoordXY;
            }
            else if (e.GetType() == typeof(CoordDegree))
            {
                CoordDegree modelDegree = e as CoordDegree;
            }
            else if (e.GetType() == typeof(CoordDdMmSs))
            {
                CoordDdMmSs modelDdMmSs = e as CoordDdMmSs;
            }
            MessageBox.Show($"{sender.ToString()} On Execute! Model: {e.GetType()}");
        }

        private void TestAzimutTask_OnClear(object sender, EventArgs e)
        {
            MessageBox.Show($"{sender.ToString()} On Clear!");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (enumSysCoord == EnumSysCoord.XY) enumSysCoord = EnumSysCoord.DdMmSs;
            else if (enumSysCoord == EnumSysCoord.DdMmSs) enumSysCoord = EnumSysCoord.Degree;
            else if (enumSysCoord == EnumSysCoord.Degree) enumSysCoord = EnumSysCoord.XY;
            testAzimutTask.ChangeSystemCoord(enumSysCoord);
            SetResourcelanguage(Languages.Eng);
        }

        private void SetResourcelanguage(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri("/Test;component/DictionaryContent.EN.xaml",
                                      UriKind.Relative);
                        break;

                    case Languages.Rus:
                        dict.Source = new Uri("/Test;component/DictionaryContent.RU.xaml",
                                           UriKind.Relative);
                        break;

                    case Languages.Az:
                        dict.Source = new Uri("/Test;component/DictionaryContent.AZ.xaml",
                                           UriKind.Relative);
                        break;

                    default:
                        dict.Source = new Uri("/Test;component/DictionaryContent.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }

        Languages languages = Languages.Eng;

        EnumSysCoord enumSysCoord = EnumSysCoord.XY;
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (languages == Languages.Az)
                languages = Languages.Rus;
            else
                languages = Languages.Az;

            testAzimutTask.ChangeLanguge(languages);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            CoordXY coordXY = new CoordXY
            {
                Altitude = 75,
                X = 85,
                Y = 95
            };
            testAzimutTask.SetModel(coordXY);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            CoordDegree coordDegree = new CoordDegree
            {
                Altitude = 35,
                Latitude = 45,
                Longitude = 55
            };
            testAzimutTask.SetModel(coordDegree);
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            CoordDdMmSs coordDdMmSs = new CoordDdMmSs
            {
                Altitude = 97,
                Longitude = new ItemCoordDdMmSs()
                {
                    Degree = 37,
                    Minutes = 47,
                    Seconds = 57
                },
                Latitude = new ItemCoordDdMmSs()
                {
                    Degree = 15,
                    Minutes = 16,
                    Seconds = 17
                }
            };
            testAzimutTask.SetModel(coordDdMmSs);
        }
    }
}
